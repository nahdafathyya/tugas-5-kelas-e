package tugas5GUI;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

public class GUI extends JFrame {

    private JTextField namaField;
    private JTextField nimField;
    private JTextField universitasField;
    private DefaultTableModel tableModel;
    private JTable table;

    public GUI() {
        setTitle("Form Tugas5");
        setSize(600, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        // Form Panel
        JPanel formPanel = new JPanel(new GridLayout(4, 2));

        formPanel.add(new JLabel("Nama:"));
        namaField = new JTextField();
        formPanel.add(namaField);

        formPanel.add(new JLabel("NIM:"));
        nimField = new JTextField();
        formPanel.add(nimField);

        formPanel.add(new JLabel("Universitas:"));
        universitasField = new JTextField();
        formPanel.add(universitasField);

        JButton submitButton = new JButton("Submit");
        formPanel.add(submitButton);

        JButton deleteButton = new JButton("Delete");
        formPanel.add(deleteButton);

        add(formPanel, BorderLayout.NORTH);

        // Table Panel
        tableModel = new DefaultTableModel(new String[]{"ID", "Nama", "NIM", "Universitas"}, 0);
        table = new JTable(tableModel);
        JScrollPane tableScrollPane = new JScrollPane(table);
        add(tableScrollPane, BorderLayout.CENTER);

        // Button Action for Submit
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nama = namaField.getText();
                String nim = nimField.getText();
                String universitas = universitasField.getText();
                if (nama.isEmpty() || nim.isEmpty() || universitas.isEmpty()) {
                    JOptionPane.showMessageDialog(GUI.this, "Please fill all fields!", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    saveData(nama, nim, universitas);
                    loadData();
                }
            }
        });

        // Button Action for Delete
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow();
                if (selectedRow >= 0) {
                    int id = (int) tableModel.getValueAt(selectedRow, 0);
                    deleteData(id);
                    loadData();
                } else {
                    JOptionPane.showMessageDialog(GUI.this, "Please select a row to delete!", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        // Load data when starting
        loadData();
    }

    private void saveData(String nama, String nim, String universitas) {
        Connection connection = DBConnection.getConnection();
        if (connection != null) {
            String sql = "INSERT INTO mahasiswa (nama, nim, universitas) VALUES (?, ?, ?)";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setString(1, nama);
                preparedStatement.setString(2, nim);
                preparedStatement.setString(3, universitas);
                preparedStatement.executeUpdate();
                JOptionPane.showMessageDialog(this, "Data successfully saved!");
            } catch (SQLException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "Failed to save data: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Failed to connect to database.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void deleteData(int id) {
        Connection connection = DBConnection.getConnection();
        if (connection != null) {
            String sql = "DELETE FROM mahasiswa WHERE id = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, id);
                preparedStatement.executeUpdate();
                JOptionPane.showMessageDialog(this, "Data successfully deleted!");
            } catch (SQLException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "Failed to delete data: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Failed to connect to database.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void loadData() {
        Connection connection = DBConnection.getConnection();
        if (connection != null) {
            String sql = "SELECT * FROM mahasiswa";
            try (Statement statement = connection.createStatement();
                 ResultSet resultSet = statement.executeQuery(sql)) {

                // Clear existing data
                tableModel.setRowCount(0);

                // Load data from result set
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String nama = resultSet.getString("nama");
                    String nim = resultSet.getString("nim");
                    String universitas = resultSet.getString("universitas");
                    tableModel.addRow(new Object[]{id, nama, nim, universitas});
                }
            } catch (SQLException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "Failed to load data: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Failed to connect to database.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new GUI().setVisible(true);
            }
        });
    }
}
